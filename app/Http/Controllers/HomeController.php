<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index () { return view('index'); }
    public function about_us () { return view('about_us'); }
    public function blog () { return view('blog'); }
    public function course_details () { return view('course-details'); }
    public function courses () { return view('courses'); }
    public function single_blog () { return view('single_blog'); }
    public function contact () { return view('contact'); }
    public function elements () { return view('elements'); }
    public function insert () { return view('insert'); }
}
