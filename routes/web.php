<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', 'HomeController@index')->name('index');
Route::get('/about_us', 'HomeController@about_us')->name('about_us');
Route::get('/blog', 'HomeController@blog')->name('blog');
Route::get('/course_details', 'HomeController@course')->name('course_details');
Route::get('/courses', 'HomeController@courses')->name('courses');
Route::get('/single_blog', 'HomeController@single_blog')->name('single_blog');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/elements', 'HomeController@elements')->name('elements');

Route::get('/student', 'HomeController@form');
Route::post('/studentinsert', 'HomeController@insert');

Auth::routes(); // autentifikatsiya route lari

Route::get('/home', 'HomeController@index')->name('home');
